<?php

/** @author Kirill A.Lapchinsky rumatakira74@gmail.com
 *  @copyright 2019 Kirill A.Lapchinsky All Rights Reserved
 */

$attempts = 1000;
const START_ACCOUNT = 135;
static $saveAccount = 0;
static $outOfMoneyCounter = 0;
for ($j = 0; $j <= $attempts; $j++) {
    static $account = START_ACCOUNT;
    static $bet = 0;
    static $zerroBet = 0;
    static $zerroCounter = 0;
    static $blackCounter = 0;
    $iterations = 50;
    $red_array = [1, 3, 5, 7, 9, 12, 14, 16, 18, 19, 21, 23, 25, 27, 30, 32, 34, 36];
    $blackArray = [2, 4, 6, 8, 10, 11, 13, 15, 17, 20, 22, 24, 26, 28, 29, 31, 33, 35];
    $bet = 0.2;
    $zerroBet = 0.1;

    for ($i = 0; $i <= $iterations; $i++) {
        $rezultArray = null;

        // generating number & check for 0, red and black
        $rezultArray[0] = random_int(0, 36);
        if ($rezultArray[0] == 0) {
            $rezultArray[1] = "zerro";
            ++$zerroCounter;
            $blackCounter = 0;
        } elseif (in_array($rezultArray[0], $red_array)) {
            $rezultArray[1] = "red";
            $blackCounter = 0;
        } else {
            $rezultArray[1] = "black";
            ++$blackCounter;
        }

        // making bet if zerro and red
        if ($rezultArray[1] == "zerro") {
            $account = $account - ($zerroBet + $bet);
            $account = $account + ($zerroBet * 35);
            if ($account >= (START_ACCOUNT * 2)) {
                $saveAccount = $saveAccount + ($account - START_ACCOUNT);
                $account = START_ACCOUNT;
            }
            $bet = 0.2;
            $zerroBet = 0.1;
        } elseif ($rezultArray[1] == "red") {
            $account = $account - ($zerroBet + $bet);
            $account =  $account + ($bet * 2);
            if ($account >= (START_ACCOUNT * 2)) {
                $saveAccount = $saveAccount + ($account - START_ACCOUNT);
                $account = START_ACCOUNT;
            }
            $bet = 0.2;
            $zerroBet = 0.1;
            // making bet if black
        } else {
            if (($zerroBet * 35) < ($zerroBet + $bet)) {
                $zerroBet = round($zerroBet * 2, 2, PHP_ROUND_HALF_UP);
            }
            $sum = $zerroBet + $bet;
            $account = round($account - $sum, 2, PHP_ROUND_HALF_UP);
            $bet = $bet * 2;
            if ($account <= 0) {
                if ($saveAccount == 0) {
                    echo "\e[31mOUT OF MONEY\e[0m at attempt " . $j . "\n";
                    echo "total accout balance: " . $account . "\n";
                    echo "save accout balance: " . $saveAccount . "\n";
                    echo "total consecutive black: " . $blackCounter . "\n";
                    ++$outOfMoneyCounter;
                } else {
                    $account = START_ACCOUNT;
                    $saveAccount = $saveAccount - START_ACCOUNT;
                }
            }
        }
    }
}
echo "\e[33mEND\n";
echo "total accout balance: " . $account . "\n";
echo "save accout balance: " . $saveAccount . "\n";
echo "out of money counter: " . $outOfMoneyCounter . "\n\e[0m";
